package com.alibaba.cloud.examples.tcc;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 17:49 2020-12-03
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
public class ResultHolder {
    private static Map<Class<?>, Map<String, String>> map = new ConcurrentHashMap<Class<?>, Map<String, String>>();

    public static void setResult(Class<?> actionClass, String xid, String v) {
        Map<String, String> results = map.get(actionClass);

        if (results == null) {
            synchronized (map) {
                if (results == null) {
                    results = new ConcurrentHashMap<>();
                    map.put(actionClass, results);
                }
            }
        }

        results.put(xid, v);
    }

    public static String getResult(Class<?> actionClass, String xid) {
        Map<String, String> results = map.get(actionClass);
        if (results != null) {
            return results.get(xid);
        }

        return null;
    }

    public static void removeResult(Class<?> actionClass, String xid) {
        Map<String, String> results = map.get(actionClass);
        if (results != null) {
            results.remove(xid);
        }
    }
}
