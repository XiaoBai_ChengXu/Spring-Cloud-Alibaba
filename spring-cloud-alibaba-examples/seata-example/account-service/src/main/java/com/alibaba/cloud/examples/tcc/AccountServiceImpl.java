package com.alibaba.cloud.examples.tcc;

import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 17:46 2020-12-04
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountTccAction accountTccAction;

    @GlobalTransactional
    @Override
    public void purchase(String userId, Integer money) {
        accountTccAction.prepareDecreaseAccount(null,userId,money);
    }
}
