package com.alibaba.cloud.examples.tcc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 11:14 2020-12-04
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
@RestController
public class TccAccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/tcc_account", produces = "application/json")
    public String account(String userId, Integer money) {
        accountService.purchase(userId,money);
        return "SUCCESS";
    }
}
