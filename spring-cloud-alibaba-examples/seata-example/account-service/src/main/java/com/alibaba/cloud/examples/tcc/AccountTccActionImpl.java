package com.alibaba.cloud.examples.tcc;

import com.google.common.collect.ImmutableMap;
import io.seata.rm.tcc.api.BusinessActionContext;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 17:05 2020-12-04
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
@Component
public class AccountTccActionImpl implements AccountTccAction {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(AccountTccActionImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public boolean prepareDecreaseAccount(BusinessActionContext businessActionContext, String userId, Integer money) {
        log.info("减少账户金额，第一阶段锁定金额，userId="+userId+"， money="+money);
        Account account = namedParameterJdbcTemplate.queryForObject("SELECT * FROM account_tbl WHERE user_id = :userId", ImmutableMap.of("userId", userId), new BeanPropertyRowMapper<Account>(Account.class));
        if (account.getMoney().compareTo(money) < 0) {
            throw new RuntimeException("账户金额不足");
        }
        /*
        余额-money
        冻结+money
         */
        //
        ImmutableMap<String, ? extends Serializable> map = ImmutableMap.of("userId", userId, "money", money);
        int i = namedParameterJdbcTemplate.update("UPDATE account_tbl SET money = (ifnull(money,0)-ifnull(:money,0)),frozen = (ifnull(frozen,0)+ifnull(:money,0)) WHERE user_id =:userId", map);
//        if (Math.random() < 0.999) {
//            throw new RuntimeException("模拟异常");
//        }
        //保存标识
        ResultHolder.setResult(getClass(), businessActionContext.getXid(), "p");
        return true;
    }

    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public boolean commit(BusinessActionContext businessActionContext) {

        String userId = businessActionContext.getActionContext("userId").toString();
        Integer money =  new Integer(businessActionContext.getActionContext("money").toString());
        log.info("减少账户金额，第二阶段，提交，userId="+userId+"， money="+money);

        //防止重复提交
        if (ResultHolder.getResult(getClass(), businessActionContext.getXid()) == null) {
            return true;
        }

        ImmutableMap<String, ? extends Serializable> map = ImmutableMap.of("userId", userId, "money", money);
        int i = namedParameterJdbcTemplate.update("UPDATE account_tbl SET frozen = (ifnull(frozen,0)-ifnull(:money,0)),used = (ifnull(used,0)+ifnull(:money,0)) WHERE user_id =:userId", map);
        double random = Math.random();
//        System.out.println(random +"====="+(random<0.99)+"");
//        if ( random<0.99) {
//            throw new RuntimeException("第二阶段模拟异常");
//        }
        //删除标识
        ResultHolder.removeResult(getClass(), businessActionContext.getXid());
        return true;
    }

    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public boolean rollback(BusinessActionContext businessActionContext) {
        String userId = businessActionContext.getActionContext("userId").toString();
        Integer money =  new Integer(businessActionContext.getActionContext("money").toString());

        //防止重复提交
        if (ResultHolder.getResult(getClass(), businessActionContext.getXid()) == null) {
            return true;
        }

        log.info("减少账户金额，第二阶段，回滚，userId="+userId+"， money="+money);

        ImmutableMap<String, ? extends Serializable> map = ImmutableMap.of("userId", userId, "money", money);
        int i = namedParameterJdbcTemplate.update("UPDATE account_tbl SET frozen = (ifnull(frozen,0)-ifnull(:money,0)),money = (ifnull(money,0)+ifnull(:money,0)) WHERE user_id =:userId", map);

        //删除标识
        ResultHolder.removeResult(getClass(), businessActionContext.getXid());
        return true;
    }
}