package com.alibaba.cloud.examples.tcc;


/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 11:03 2020-12-04
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
public interface AccountService {

     /**
      * 用户付款
      * @param userId
      * @param money
      */
     void purchase(String userId, Integer money);
}
