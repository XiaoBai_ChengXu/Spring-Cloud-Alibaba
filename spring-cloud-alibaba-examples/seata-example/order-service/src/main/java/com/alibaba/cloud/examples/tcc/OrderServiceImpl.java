package com.alibaba.cloud.examples.tcc;

import com.alibaba.cloud.examples.Order;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 11:05 2020-12-04
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderTccAction orderTccAction;


    @GlobalTransactional
    @Override
    public void create(Order order) {
        orderTccAction.prepareCreateOrder(null,order);
    }
}
