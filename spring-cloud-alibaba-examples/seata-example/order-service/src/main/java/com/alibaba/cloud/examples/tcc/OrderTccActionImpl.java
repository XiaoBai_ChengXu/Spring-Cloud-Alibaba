package com.alibaba.cloud.examples.tcc;

import com.alibaba.cloud.examples.Order;
import com.alibaba.cloud.examples.OrderController;
import io.seata.rm.tcc.api.BusinessActionContext;
import jdk.nashorn.internal.runtime.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 17:48 2020-12-03
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
@Component
public class OrderTccActionImpl implements OrderTccAction{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(OrderTccActionImpl.class);


    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public boolean prepareCreateOrder(BusinessActionContext businessActionContext,Order order) {
        log.info("创建 order 第一阶段，预留资源 - "+businessActionContext.getXid());
        order.setStatus("0");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        if (true){
            throw new RuntimeException("模拟出错");
        }
        int result = jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection con)
                    throws SQLException {
                PreparedStatement pst = con.prepareStatement(
                        "insert into order_tbl (user_id, commodity_code, count, money,status) values (?, ?, ?, ?,?)",
                        PreparedStatement.RETURN_GENERATED_KEYS);
                pst.setObject(1, order.userId);
                pst.setObject(2, order.commodityCode);
                pst.setObject(3, order.count);
                pst.setObject(4, order.money);
                pst.setObject(5, order.status);
                return pst;
            }
        }, keyHolder);
        order.id = keyHolder.getKey().longValue();
        //事务成功，保存一个标识，供第二阶段进行判断
        ResultHolder.setResult(getClass(), businessActionContext.getXid(), order.id+"");
//        if (Math.random() < 0.999) {
//            throw new RuntimeException("一阶段模拟异常");
//        }
        return true;
    }

    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public boolean commit(BusinessActionContext businessActionContext) {
        log.info("创建 order 第二阶段提交，修改订单状态1 - "+businessActionContext.getXid());

        // 防止幂等性，如果commit阶段重复执行则直接返回
        String result1 = ResultHolder.getResult(getClass(), businessActionContext.getXid());
        if (result1 == null) {
            return true;
        }
        int result = jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con)
                    throws SQLException {
                PreparedStatement pst = con.prepareStatement(
                        "update order_tbl set status = ? WHERE id = ?");
                pst.setObject(1, "1");
                pst.setObject(2, result1);
                return pst;
            }
        });
        //提交成功是删除标识
        ResultHolder.removeResult(getClass(), businessActionContext.getXid());
        return true;
    }

    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public boolean rollback(BusinessActionContext businessActionContext) {
        log.info("创建 order 第二阶段回滚，删除订单 - "+businessActionContext.getXid());

        //第一阶段没有完成的情况下，不必执行回滚
        //因为第一阶段有本地事务，事务失败时已经进行了回滚。
        //如果这里第一阶段成功，而其他全局事务参与者失败，这里会执行回滚
        //幂等性控制：如果重复执行回滚则直接返回
        String result1 = ResultHolder.getResult(getClass(), businessActionContext.getXid());
        if (result1 == null) {
            return true;
        }
        //long orderId = Long.parseLong(businessActionContext.getActionContext("orderId").toString());
        int result = jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con)
                    throws SQLException {
                PreparedStatement pst = con.prepareStatement(
                        "DELETE from order_tbl  WHERE id = ?");
                pst.setObject(1, result1);
                return pst;
            }
        });
        //回滚结束时，删除标识
        ResultHolder.removeResult(getClass(), businessActionContext.getXid());
        return true;
    }
}
