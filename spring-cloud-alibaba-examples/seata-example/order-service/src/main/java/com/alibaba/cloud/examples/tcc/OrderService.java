package com.alibaba.cloud.examples.tcc;

import com.alibaba.cloud.examples.Order;

/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 11:03 2020-12-04
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
public interface OrderService {

     void create(Order order);
}
