package com.alibaba.cloud.examples.tcc;

import com.alibaba.cloud.examples.Order;
import io.seata.core.context.RootContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * * <pre>
 * @TODO 添加文件描述
 * </pre>
 *
 * @Author: zm
 * @Description
 * @Date: Created in 11:14 2020-12-04
 * @company 成都信通信息技术有限公司
 * @Modificd By:
 */
@RestController
public class TccOrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping(value = "/tcc_order", produces = "application/json")
    public String order(String userId, String commodityCode, int orderCount,Integer money) {
        Order order = new Order();
        order.userId = userId;
        order.commodityCode = commodityCode;
        order.count = orderCount;
        order.money = money;
        orderService.create(order);
        return order.id+"";
    }
}
