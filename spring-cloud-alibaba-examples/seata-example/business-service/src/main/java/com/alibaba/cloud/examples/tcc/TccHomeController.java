/*
 * Copyright 2013-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alibaba.cloud.examples.tcc;

import com.alibaba.cloud.examples.BusinessApplication.AccountService;
import com.alibaba.cloud.examples.BusinessApplication.OrderService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author xiaojing
 */
@RestController
public class TccHomeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TccHomeController.class);

	private static final String SUCCESS = "SUCCESS";

	private static final String FAIL = "FAIL";

	private static final String USER_ID = "U100001";

	private static final String COMMODITY_CODE = "C00321";

	private static final int ORDER_COUNT = 2;

	private final RestTemplate restTemplate;

	private final OrderService orderService;

	private final AccountService accountService;

	public TccHomeController(RestTemplate restTemplate, OrderService orderService,
							 AccountService accountService) {
		this.restTemplate = restTemplate;
		this.orderService = orderService;
		this.accountService = accountService;
	}


	@GlobalTransactional(timeoutMills = 300000, name = "spring-cloud-demo-tx")
	@GetMapping(value = "/seata/tcc_feign", produces = "application/json")
	public String Tccfeign() {

		//扣款
		String result = accountService.account(USER_ID, 500);
		System.out.println(result);
		if (!SUCCESS.equals(result)) {
			throw new RuntimeException();
		}
		//下单
		String orderId = orderService.tccOrder(USER_ID, COMMODITY_CODE, ORDER_COUNT,500);
		if (StringUtils.isBlank(orderId)) {
			throw new RuntimeException();
		}
		return SUCCESS;

	}

}
